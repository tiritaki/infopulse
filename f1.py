def f():
    print("part1")
    yield "fff"
    print("part2")
    yield "shsgh"

def main():
    # gen=f()
    # res1=next(gen)
    # print(res1)
    # res2=next(gen)
    # print(res2)
    # res3 = next(gen)
    for i in f():
        print(i)

main()
