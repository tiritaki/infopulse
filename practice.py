
class Accessories:
    def __init__(self, price, paper, string):
        self.price=price
        self.paper = paper
        self.string = string

    def cost_acc(self):
        return self.paper.price + self.string.price

white=Accessories(10,200,'white')
blue=Accessories(21,300,'blue')

acc=Accessories(1,white,blue)
print(acc.cost_acc())