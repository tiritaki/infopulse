class A:
    def __init__(self):
        self.__dict__["field1"]=10
        self.field2=20

def main():
    pa=A()
    pa.field1=500
    pa.field2=900
    print(pa.field1)
    print(pa.__dict__)
    pa1=A()
    pa1.field1=700
    pa1.field2=800
    print(pa1.__dict__)

main()

