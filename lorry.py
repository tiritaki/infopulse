class Coffee:
    def __init__(self, price, sort):
        self.price = price
        self.sort = sort


class Box:
    def __init__(self, weight, coffee):
        self.weight = weight
        self.coffee = coffee

    def cost(self):
        return self.weight * self.coffee.price


class Lorry:
    def __init__(self):
        self.boxes = []

    def add_box(self, box):
        self.boxes.append(box)

    def total_cost(self):
        total = 0
        for box in self.boxes:
            total += box.cost()
        return total


arabica = Coffee(50, 'arabica')
robusta = Coffee(30, 'robusta')

box1 = Box(20, arabica)
box2 = Box(30, robusta)
box3 = Box(36, arabica)

lorry = Lorry()
lorry.add_box(box1)
lorry.add_box(box2)
lorry.add_box(box3)

total = lorry.total_cost()
print(total)