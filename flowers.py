class Bouquet:
    def __init__(self):
        self.flowers = []

    def add_flowers(self, flower):
        self.flowers.append(flower)

    def total_cost(self):
        total = 0
        for flower in self.flowers:
            total += flower.cost()
        return total+acc.cost_acc()

class Flowers:
    def __init__(self, price, sort):
        self.price = price
        self.sort = sort

    def cost(self):
        return self.sort.price


class Accessories:
    def __init__(self, price, paper, string):
        self.price=price
        self.paper = paper
        self.string = string

    def cost_acc(self):
        price_acc=self.paper.price + self.string.price
        return price_acc


white=Accessories(50,200,'white')
blue=Accessories(6,300,'blue')


acc=Accessories(1,white,blue)

rose = Flowers(15, 'rose')
tulip = Flowers(10, 'tulip')
narcissus = Flowers(20, 'narcissus')
sunflower = Flowers(19, 'sunflower')


flower1 = Flowers(1, rose )
flower2 = Flowers(1, tulip)
flower3 = Flowers(1, narcissus)
flower4 = Flowers(1, sunflower)

bouquet = Bouquet()

bouquet.add_flowers(flower1)
bouquet.add_flowers(flower2)
bouquet.add_flowers(flower3)
bouquet.add_flowers(flower4)

total = bouquet.total_cost()

print(total)