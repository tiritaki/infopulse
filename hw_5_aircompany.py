class Flight:
    def __init__(self,airplane,direction):
        self.airplane = airplane
        self.direction = direction
        self.staff = []
        self.tickets = []
        self.weight_c = 0

    def add_staff(self,stf):
        self.staff.append(stf)

    def add_ticket_pas(self,ticket):
        if len(self.tickets)<= self.airplane.places:
            self.tickets.append(ticket)
        else:
            print('limit was exceeded')

    def add_cargo_ticket(self,ticket):
        if self.weight_c <= self.airplane.max_cargo:
            self.tickets.append(ticket)
            self.weight_c += ticket.weight


    def income_pas(self):
        total = 0
        for ticket in self.tickets:
            total += ticket.price

        for stf in self.staff:
            total -= stf.salary

        total -= self.direction.distance*self.airplane.fuel*100 #100 - price for a litre
        return total
class Airplane:
    def __init__(self,places,fuel):
        self.places = places
        self.fuel = fuel


class Airplane_cargo:
    def __init__(self,fuel,max_cargo):
        self.fuel = fuel
        self.max_cargo = max_cargo

class Staff:
    def __init__(self,salary):
        self.salary = salary


class Ticket_pas:
    def __init__(self,price):
        self.price = price

class Ticket_cargo:
    def __init__(self,price_kg,weight):
        self.price_kg = price_kg
        self.weight = weight
        self.price = weight *price_kg

class Direction:
    def __init__(self,distance):
        self.distance = distance


class Aircompany:
    def __init__(self):
        self.flights = []

    def add_flight(self,flight):
        self.flights.append(flight)

    def profit(self):
        total = 0
        for flight in self.flights:
            total += flight.income_pas()

        return total
        #Добавить прибыль от груза




aircompany = Aircompany()
airplane = Airplane(30,24)
airplane2 = Airplane_cargo(28,2800)
direction = Direction(800)
direction2 = Direction(1200)
flight1 = Flight(airplane,direction)
flight2 = Flight(airplane2,direction2)
staff1 = Staff(300)
staff2 = Staff(600)

flight1.add_staff(staff1)
flight1.add_staff(staff2)

flight2.add_staff(staff1)


ticket1 = Ticket_pas(400)
ticket2 = Ticket_pas(500)
ticket3 = Ticket_pas(1000000)

cargo_ticket1 = Ticket_cargo(2,30088)
cargo_ticket2 = Ticket_cargo(4,720)

flight1.add_ticket_pas(ticket1)
flight1.add_ticket_pas(ticket2)
flight1.add_ticket_pas(ticket3)
flight2.add_cargo_ticket(cargo_ticket1)
flight2.add_cargo_ticket(cargo_ticket2)

aircompany.add_flight(flight1)
aircompany.add_flight(flight2)

res = aircompany.profit()
print(res)
