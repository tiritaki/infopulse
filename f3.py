def f():
    print("part1")
    j=yield 1
    print(j)
    print("part2")
    k=yield 2
    print(k)

gen=f()
res1=next(gen)
res2=gen.send(100)
print(res2)
res3=gen.send(300)

