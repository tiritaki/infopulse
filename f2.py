def myrange(begin,end):
    temp=begin
    while(temp<end):
        yield temp
        temp+=1

for i in myrange(0,10):
    print(i)

gen=myrange(0,10)
r1=next(gen)